alter table player
    add id_team integer;

comment on column player.id_team is 'Команда';

alter table player
    add constraint player_team_id_fk
        foreign key (id_team) references team;

comment on constraint player_team_id_fk on player is 'Внешний ключ на команду игрока';

alter table player
    drop column if exists team;

