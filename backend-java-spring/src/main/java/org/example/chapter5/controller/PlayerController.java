package org.example.chapter5.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.example.chapter5.model.Player;
import org.example.chapter5.model.Team;
import org.example.chapter5.repository.PlayerRepo;
import org.example.chapter5.repository.TeamRepo;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("players")
@RequiredArgsConstructor
@Log4j2
@Tag(name = "Player API")
public class PlayerController {
    private final PlayerRepo playerRepo;
    private final TeamRepo teamRepo;

    @GetMapping
    public List<Player> getAll() {
        List<Player> players = playerRepo.findAll();

        return players;
    }

    @GetMapping("{id}")
    public Player getById(@PathVariable(name = "id") Player player) {
        return player;
    }

    @PostMapping
    public Player create(@RequestBody final Player playerDto) {
        Player createdPlayer = null;

        try {
            Team team = teamRepo.findById(playerDto.getTeam().getId()).get();
            playerDto.setTeam(team);
            createdPlayer = playerRepo.save(playerDto);
        } catch (Exception e) {
            log.error("Ошибка при создании игрока", e.getMessage());
        }

        return createdPlayer;
    }

    @PutMapping("{id}")
    public Player update(@PathVariable(name = "id") Player player, @RequestBody final Player playerDto) {
        try {
            Team team = teamRepo.findById(playerDto.getTeam().getId()).get();

            player.setNickname(playerDto.getNickname());
            player.setPosition(playerDto.getPosition());
            player.setTeam(team);

            playerRepo.save(player);
        } catch (Exception e) {
            log.error("Ошибка при обновлении игрока с id " + player.getId(), e.getMessage());
        }

        return player;
    }

    @DeleteMapping("{id}")
    public Player delete(@PathVariable(name = "id") Player player) {
        try {
            playerRepo.delete(player);
        } catch (Exception e) {
            log.error("Ошибка при удалении игрока с id " + player.getId(), e.getMessage());
        }

        return player;
    }
}
