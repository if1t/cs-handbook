package org.example.chapter5.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.example.chapter5.model.Team;
import org.example.chapter5.repository.TeamRepo;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("teams")
@RequiredArgsConstructor
@Log4j2
@Tag(name = "Team API")
public class TeamController {
    private final TeamRepo teamRepo;

    @GetMapping
    public List<Team> getAll() {
        List<Team> teams = teamRepo.findAll();

        return teams;
    }

    @GetMapping("{id}")
    public Team getById(@PathVariable(name = "id") Team team) {
        return team;
    }

    @PostMapping
    public Team create(@RequestBody final Team team) {
        Team savedTeam = null;

        try {
            savedTeam = teamRepo.save(team);
        } catch (Exception e) {
            log.error("Ошибка при создании команды", e.getMessage());
        }

        return savedTeam;
    }

    @PutMapping("{id}")
    public Team update(@PathVariable(name = "id") Team team, @RequestBody final Team teamDto) {
        Team updatedTeam = null;

        try {
            team.setName(teamDto.getName());
            team.setDescription(teamDto.getDescription());

            updatedTeam = teamRepo.save(team);
        } catch (Exception e) {
            log.error("Ошибка при обновлении команды с id " + team.getId(), e.getMessage());
        }

        return updatedTeam;
    }

    @DeleteMapping("{id}")
    public Team delete(@PathVariable(name = "id") Team team) {

        try {
            teamRepo.delete(team);
        } catch (Exception e) {
            log.error("Ошибка при удалении команды с id " + team.getId(), e.getMessage());
        }

        return team;
    }
}
