package org.example.chapter5.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

/** Команда */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Entity
@Table(name = "team", schema = "public")
public class Team {

    /** Идентификатор команды */
    @Id
    @Column(name = "id", nullable = false)
    @SequenceGenerator(name = "TEAM_SEQ_GEN", sequenceName = "team_id_seq", allocationSize = 1, schema = "public")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEAM_SEQ_GEN")
    private Long id;

    /** Название команды */
    private String name;

    /** Описание команды */
    private String description;

    @OneToMany(mappedBy = "team")
    @JsonBackReference
    private Set<Player> players;

    public void printInfo() {
        System.out.println("Название: ".concat(name));
        System.out.println("Описание: ".concat(description));
    }
}
