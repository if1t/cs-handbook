package org.example.chapter5.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


/** Профиль игрока */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Entity
@Table(name = "player", schema = "public")
public class Player {

    /** Идентификатор игрока */
    @Id
    @Column(name = "id", nullable = false)
    @SequenceGenerator(name = "PLAYER_SEQ_GEN", sequenceName = "player_id_seq", allocationSize = 1, schema = "public")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PLAYER_SEQ_GEN")
    private Long id;

    /** Никнейм */
    private String nickname;

    /** Команда игрока */
    @ManyToOne
    @JoinColumn(name="id_team")
    private Team team;

    /** Позиция в игре */
    private String position;

    public void printInfo() {
        System.out.println("Ник: ".concat(nickname));
        System.out.println("Позиция: ".concat(position));
        System.out.println("Команда: ");
        team.printInfo();
    }
}
