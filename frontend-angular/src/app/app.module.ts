import { NgModule } from '@angular/core';
import { TuiRootModule } from '@taiga-ui/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  declarations: [],
  imports: [BrowserModule, BrowserAnimationsModule, TuiRootModule],
  providers: [HttpClientModule],
})
export class AppModule {}
