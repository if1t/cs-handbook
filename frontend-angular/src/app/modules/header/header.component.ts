import { Component } from '@angular/core';
import { TaigaUiSharedModule } from '../../shared/taiga-ui.shared.module';
import { BaseImportsSharedModule } from '../../shared/base-imports.shared.module';
import { AuthService } from '../../shared/services/auth.service';
import { IUser, UserService } from '../../shared/services/user.service';
import { Router, RouterLink } from '@angular/router';

@Component({
  selector: 'cs-header',
  standalone: true,
  imports: [TaigaUiSharedModule, BaseImportsSharedModule, RouterLink],
  templateUrl: './header.component.html',
  styleUrl: './header.component.css',
})
export class HeaderComponent {
  protected isOpenProfileDropdown = false;

  constructor(
    private readonly _authService: AuthService,
    private readonly _userService: UserService,
    private readonly _router: Router,
  ) {}

  protected logout(): void {
    this._authService.logout();
  }

  protected get user(): IUser | null {
    return this._userService.getUser();
  }

  protected navigateToCabinet(): void {
    this.isOpenProfileDropdown = false;
    void this._router.navigate(['cabinet']);
  }

  protected navigateToHistoryBack(): void {
    history.back();
  }
}
