import { Component, OnInit } from '@angular/core';
import { BaseImportsSharedModule } from '../../shared/base-imports.shared.module';
import { TuiInputPasswordModule } from '@taiga-ui/kit';
import { TaigaUiSharedModule } from '../../shared/taiga-ui.shared.module';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../shared/services/auth.service';
import { ICredentials } from './login.schema';
import { Router, RouterLink } from '@angular/router';

@Component({
  selector: 'cs-login',
  standalone: true,
  imports: [BaseImportsSharedModule, TuiInputPasswordModule, TaigaUiSharedModule, RouterLink],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css',
})
export class LoginComponent implements OnInit {
  constructor(
    private readonly _authService: AuthService,
    private readonly _router: Router,
  ) {}

  public ngOnInit(): void {
    if (this._authService.isAuthenticated) {
      void this._router.navigate(['players']);
    }
  }

  protected loginForm = new FormGroup({
    username: new FormControl<string>('', Validators.required),
    password: new FormControl<string>('', Validators.required),
  });

  protected login(): void {
    const credential = this.loginForm.getRawValue() as ICredentials;
    this._authService.login(credential);
  }
}
