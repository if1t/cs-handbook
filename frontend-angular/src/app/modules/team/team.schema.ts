import { IPlayer } from '../player/player.schema';

export interface ITeam {
  id?: number;
  name?: string;
  players?: IPlayer[];
  description?: string;
}
