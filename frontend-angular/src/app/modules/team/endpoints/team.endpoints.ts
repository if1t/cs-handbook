import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ITeam } from '../team.schema';

export const TEAMS_ENDPOINTS = '/api/teams';

@Injectable({
  providedIn: 'root',
})
export class TeamEndpoints {
  constructor(private readonly http: HttpClient) {}

  public getAll(): Observable<ITeam[]> {
    return this.http.get<ITeam[]>(TEAMS_ENDPOINTS);
  }

  public getById(id: number): Observable<ITeam> {
    return this.http.get<ITeam>(`${TEAMS_ENDPOINTS}/${id}`);
  }

  public create(player: ITeam): Observable<ITeam> {
    return this.http.post<ITeam>(TEAMS_ENDPOINTS, player);
  }

  public update(id: number, player: ITeam): Observable<ITeam> {
    return this.http.put<ITeam>(`${TEAMS_ENDPOINTS}/${id}`, player);
  }

  public delete(id: number): Observable<ITeam> {
    return this.http.delete<ITeam>(`${TEAMS_ENDPOINTS}/${id}`);
  }
}
