import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, catchError, delay, tap } from 'rxjs';
import { AlertService } from '../../../shared/services/alert.service';
import { ITeam } from '../team.schema';
import { TeamEndpoints } from '../endpoints/team.endpoints';

@Injectable()
export class TeamStoreService {
  private _teams$: BehaviorSubject<ITeam[]> = new BehaviorSubject<ITeam[]>([]);
  public teams$: Observable<ITeam[]> = this._teams$.asObservable();

  private _loading$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public loading$: Observable<boolean> = this._loading$.asObservable();

  constructor(
    private readonly _endpoints: TeamEndpoints,
    private readonly _alertService: AlertService,
  ) {}

  public fetchAll(): void {
    this._setLoading(true);

    this._endpoints
      .getAll()
      .pipe(delay(1500))
      .subscribe((teams) => {
        this._teams$.next(teams);
        this._setLoading(false);
      });
  }

  public getById(id: number): Observable<ITeam> {
    return this._endpoints.getById(id).pipe(delay(1500));
  }

  public create(team: ITeam): Observable<ITeam> {
    this._setLoading(true);

    return this._endpoints.create(team).pipe(
      delay(1500),
      tap((createdTeam: ITeam) => {
        const teams = this._teams$.getValue();
        teams.push(createdTeam);

        this._teams$.next(teams);
        this._setLoading(false);
        this._alertService.notify('Команда успешно создана!');
      }),
      catchError((error) => {
        this._alertService.notify('Ошибка при создании команды', 'error');
        throw new Error(error);
      }),
    );
  }

  public update(team: ITeam): Observable<ITeam> {
    this._setLoading(true);

    return this._endpoints.update(team.id!, team).pipe(
      delay(1500),
      tap((updatedteam: ITeam) => {
        const teams = this._teams$.getValue();
        const teamIndex = teams.findIndex((team) => team.id === updatedteam.id);

        if (teamIndex === -1) {
          throw new Error(`team with id ${updatedteam.id} not found`);
        }

        teams[teamIndex] = updatedteam;

        this._teams$.next(teams);
        this._setLoading(false);
        this._alertService.notify('Команда успешно обновлен!');
      }),
      catchError((error) => {
        this._alertService.notify('Ошибка при обновлении команды', 'error');
        throw new Error(error);
      }),
    );
  }

  public delete(id: number): Observable<ITeam> {
    this._setLoading(true);

    return this._endpoints.delete(id).pipe(
      delay(1500),
      tap((updatedteam: ITeam) => {
        const teams = this._teams$.getValue();
        const teamIndex = teams.findIndex((team) => team.id === updatedteam.id);

        if (teamIndex === -1) {
          throw new Error(`team with id ${updatedteam.id} not found`);
        }

        teams.splice(teamIndex, 1);

        this._teams$.next(teams);
        this._setLoading(false);
        this._alertService.notify('Команда успешно удалена!');
      }),
      catchError((error) => {
        this._alertService.notify('Ошибка при удалении команды', 'error');
        throw new Error(error);
      }),
    );
  }

  private _setLoading(loading: boolean): void {
    this._loading$.next(loading);
  }
}
