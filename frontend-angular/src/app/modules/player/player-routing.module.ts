import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlayerPageComponent } from './player-page/player-page.component';
import { PlayerFormComponent } from './player-form/player-form.component';

const routes: Routes = [
  { path: '', component: PlayerPageComponent },
  {
    path: 'form',
    component: PlayerFormComponent,
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlayerRoutingModule {}
