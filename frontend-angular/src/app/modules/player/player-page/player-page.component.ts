import { Component, OnDestroy, OnInit } from '@angular/core';
import { PlayerStoreService } from '../services/player-store.service';
import { IPlayer } from '../player.schema';
import { PolymorpheusComponent } from '@tinkoff/ng-polymorpheus';
import { TuiDialogService } from '@taiga-ui/core';
import { PlayerFormComponent } from '../player-form/player-form.component';
import { filter, Observable, Subject, switchMap, takeUntil } from 'rxjs';
import { DeleteFormComponent } from '../../../shared/components/dialogs-form/delete-form/delete-form.component';

@Component({
  selector: 'cs-player-page',
  templateUrl: './player-page.component.html',
  styleUrl: './player-page.component.css',
  providers: [PlayerStoreService],
})
export class PlayerPageComponent implements OnInit, OnDestroy {
  private readonly _unsubscribe$ = new Subject<void>();

  protected players$ = this._playerService.players$.pipe(takeUntil(this._unsubscribe$));
  protected loading$ = this._playerService.loading$.pipe(takeUntil(this._unsubscribe$));

  constructor(
    private readonly _playerService: PlayerStoreService,
    private readonly _dialogService: TuiDialogService,
  ) {}

  public ngOnInit(): void {
    this._playerService.fetchAll();
  }

  protected createPlayer(): void {
    this._openPlayerForm('Создать игрока', null)
      .pipe(
        filter<IPlayer>(Boolean),
        switchMap((player: IPlayer) => this._playerService.create(player)),
      )
      .subscribe();
  }

  protected editPlayer(player: IPlayer): void {
    this._openPlayerForm('Редактировать игрока', player)
      .pipe(
        filter<IPlayer>(Boolean),
        switchMap((editedPlayer: IPlayer) => this._playerService.update(player.id!, editedPlayer)),
      )
      .subscribe();
  }

  private _openPlayerForm(label: string, data: IPlayer | null): Observable<IPlayer> {
    return this._dialogService.open<IPlayer>(new PolymorpheusComponent(PlayerFormComponent), {
      label,
      data,
      closeable: false,
    });
  }

  protected deletePlayer(player: IPlayer): void {
    this._dialogService
      .open<boolean>(new PolymorpheusComponent(DeleteFormComponent), {
        label: 'Удаление игрока',
        data: `Вы действительно хотите удалить игрока под ником ${player.nickname}?`,
        closeable: false,
      })
      .pipe(
        filter(Boolean),
        switchMap(() => this._playerService.delete(player.id!)),
      )
      .subscribe();
  }

  public ngOnDestroy(): void {
    this._unsubscribe$.next();
    this._unsubscribe$.complete();
  }
}
