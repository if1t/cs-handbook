import { Component, Inject, OnInit } from '@angular/core';
import { TuiDialogContext } from '@taiga-ui/core';
import { POLYMORPHEUS_CONTEXT } from '@tinkoff/ng-polymorpheus';
import { IPlayer } from '../player.schema';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TeamEndpoints } from '../../team/endpoints/team.endpoints';
import { defaultStringify } from '../../../shared/components/tui-helpers/select/select.helper';
import { IItem } from '../../../shared/components/tui-helpers/select/select.schema';

@Component({
  selector: 'cs-player-form',
  templateUrl: './player-form.component.html',
  styleUrl: './player-form.component.css',
})
export class PlayerFormComponent implements OnInit {
  protected readonly defaultStringify = defaultStringify;

  protected form = this._buildForm();
  protected teamsItems: IItem[] = [];

  constructor(
    @Inject(POLYMORPHEUS_CONTEXT)
    private readonly context: TuiDialogContext<IPlayer | null, IPlayer | null>,
    private readonly _teamEndpoints: TeamEndpoints,
  ) {}

  public ngOnInit(): void {
    const player = this.context.data;

    if (player) {
      this.form.setValue({
        nickname: player.nickname,
        id_team: player.team!.id,
        position: player.position,
      });
    }

    this._fetchTeamsItems();
  }

  private _fetchTeamsItems(): void {
    this._teamEndpoints.getAll().subscribe((teams) => {
      this.teamsItems = teams.map((team) => ({ id: team.id!, name: team.name! }));
    });
  }

  protected save(): void {
    const player = this._buildPlayer(this.form.getRawValue());
    this.context.completeWith(player);
  }

  private _buildPlayer(value: any): IPlayer {
    return {
      nickname: value.nickname,
      position: value.position,
      team: {
        id: value.id_team,
      },
    };
  }

  protected cancel(): void {
    this.context.completeWith(null);
  }

  private _buildForm(): FormGroup {
    return new FormGroup({
      nickname: new FormControl<string>('', [Validators.required]),
      id_team: new FormControl<number | null>(null, [Validators.required]),
      position: new FormControl<string>('', [Validators.required]),
    });
  }
}
