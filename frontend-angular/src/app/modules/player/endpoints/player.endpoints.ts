import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IPlayer } from '../player.schema';

export const PLAYERS_ENDPOINTS = '/api/players';

@Injectable({
  providedIn: 'root',
})
export class PlayerEndpoints {
  constructor(private readonly http: HttpClient) {}

  public getAll(): Observable<IPlayer[]> {
    return this.http.get<IPlayer[]>(PLAYERS_ENDPOINTS);
  }

  public getById(id: number): Observable<IPlayer> {
    return this.http.get<IPlayer>(`${PLAYERS_ENDPOINTS}/${id}`);
  }

  public create(player: IPlayer): Observable<IPlayer> {
    return this.http.post<IPlayer>(PLAYERS_ENDPOINTS, player);
  }

  public update(id: number, player: IPlayer): Observable<IPlayer> {
    return this.http.put<IPlayer>(`${PLAYERS_ENDPOINTS}/${id}`, player);
  }

  public delete(id: number): Observable<IPlayer> {
    return this.http.delete<IPlayer>(`${PLAYERS_ENDPOINTS}/${id}`);
  }
}
