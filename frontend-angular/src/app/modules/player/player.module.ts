import { NgModule } from '@angular/core';
import { PlayerPageComponent } from './player-page/player-page.component';
import { PlayerFormComponent } from './player-form/player-form.component';
import { PlayerRoutingModule } from './player-routing.module';
import { TaigaUiSharedModule } from '../../shared/taiga-ui.shared.module';
import { BaseImportsSharedModule } from '../../shared/base-imports.shared.module';
import { DeleteFormModule } from '../../shared/components/dialogs-form/delete-form/delete-form.module';

@NgModule({
  declarations: [PlayerPageComponent, PlayerFormComponent],
  imports: [PlayerRoutingModule, BaseImportsSharedModule, TaigaUiSharedModule, DeleteFormModule],
})
export class PlayerModule {}
