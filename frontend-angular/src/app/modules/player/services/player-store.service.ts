import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, catchError, delay, tap } from 'rxjs';
import { IPlayer } from '../player.schema';
import { PlayerEndpoints } from '../endpoints/player.endpoints';
import { AlertService } from '../../../shared/services/alert.service';

@Injectable()
export class PlayerStoreService {
  private _players$: BehaviorSubject<IPlayer[]> = new BehaviorSubject<IPlayer[]>([]);
  public players$: Observable<IPlayer[]> = this._players$.asObservable();

  private _loading$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public loading$: Observable<boolean> = this._loading$.asObservable();

  constructor(
    private readonly _endpoints: PlayerEndpoints,
    private readonly _alertService: AlertService,
  ) {}

  public fetchAll(): void {
    this._setLoading(true);

    this._endpoints
      .getAll()
      .pipe(delay(1500))
      .subscribe((players) => {
        this._players$.next(players);
        this._setLoading(false);
      });
  }

  public getById(id: number): Observable<IPlayer> {
    return this._endpoints.getById(id).pipe(delay(1500));
  }

  public create(player: IPlayer): Observable<IPlayer> {
    this._setLoading(true);

    return this._endpoints.create(player).pipe(
      delay(1500),
      tap((createdPlayer: IPlayer) => {
        const players = this._players$.getValue();
        players.push(createdPlayer);

        this._players$.next(players);
        this._setLoading(false);
        this._alertService.notify('Игрок успешно создан!');
      }),
      catchError((error) => {
        this._alertService.notify('Ошибка при создании игрока', 'error');
        throw new Error(error);
      }),
    );
  }

  public update(id: number, player: IPlayer): Observable<IPlayer> {
    this._setLoading(true);

    return this._endpoints.update(id, player).pipe(
      delay(1500),
      tap((updatedPlayer: IPlayer) => {
        const players = this._players$.getValue();
        const playerIndex = players.findIndex((player) => player.id === updatedPlayer.id);

        if (playerIndex === -1) {
          throw new Error(`Player with id ${updatedPlayer.id} not found`);
        }

        players[playerIndex] = updatedPlayer;

        this._players$.next(players);
        this._setLoading(false);
        this._alertService.notify('Игрок успешно обновлен!');
      }),
      catchError((error) => {
        this._alertService.notify('Ошибка при обновлении игрока', 'error');
        throw new Error(error);
      }),
    );
  }

  public delete(id: number): Observable<IPlayer> {
    this._setLoading(true);

    return this._endpoints.delete(id).pipe(
      delay(1500),
      tap((updatedPlayer: IPlayer) => {
        const players = this._players$.getValue();
        const playerIndex = players.findIndex((player) => player.id === updatedPlayer.id);

        if (playerIndex === -1) {
          throw new Error(`Player with id ${updatedPlayer.id} not found`);
        }

        players.splice(playerIndex, 1);

        this._players$.next(players);
        this._setLoading(false);
        this._alertService.notify('Игрок успешно удален!');
      }),
      catchError((error) => {
        this._alertService.notify('Ошибка при удалении игрока', 'error');
        throw new Error(error);
      }),
    );
  }

  private _setLoading(loading: boolean): void {
    this._loading$.next(loading);
  }
}
