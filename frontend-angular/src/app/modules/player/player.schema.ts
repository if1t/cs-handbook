import { ITeam } from '../team/team.schema';

export interface IPlayer {
  id?: number;
  nickname?: string;
  team?: ITeam;
  position?: string;
}
