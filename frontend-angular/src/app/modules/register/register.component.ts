import { Component, OnInit } from '@angular/core';
import { BaseImportsSharedModule } from '../../shared/base-imports.shared.module';
import { TuiInputPasswordModule } from '@taiga-ui/kit';
import { TaigaUiSharedModule } from '../../shared/taiga-ui.shared.module';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, RouterLink } from '@angular/router';
import { ICredentials } from '../login/login.schema';
import { AuthService } from '../../shared/services/auth.service';

@Component({
  selector: 'cs-register',
  standalone: true,
  imports: [BaseImportsSharedModule, TuiInputPasswordModule, TaigaUiSharedModule, RouterLink],
  templateUrl: './register.component.html',
  styleUrl: './register.component.css',
})
export class RegisterComponent implements OnInit {
  constructor(
    private readonly _authService: AuthService,
    private readonly _router: Router,
  ) {}

  public ngOnInit(): void {
    if (this._authService.isAuthenticated) {
      void this._router.navigate(['players']);
    }
  }

  protected registerForm = new FormGroup({
    username: new FormControl<string>('', Validators.required),
    password: new FormControl<string>('', Validators.required),
  });

  protected register(): void {
    const credential = this.registerForm.getRawValue() as ICredentials;
    this._authService.register(credential);
  }
}
