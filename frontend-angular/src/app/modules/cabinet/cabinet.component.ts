import { Component } from '@angular/core';
import { UserService } from '../../shared/services/user.service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'cs-cabinet',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './cabinet.component.html',
  styleUrl: './cabinet.component.css',
})
export class CabinetComponent {
  protected user = this._userService.getUser();

  constructor(private readonly _userService: UserService) {}
}
