import { Component, Inject } from '@angular/core';
import { POLYMORPHEUS_CONTEXT } from '@tinkoff/ng-polymorpheus';
import { TuiDialogContext } from '@taiga-ui/core';

@Component({
  selector: 'cs-delete-form',
  templateUrl: './delete-form.component.html',
  styleUrl: './delete-form.component.css',
})
export class DeleteFormComponent {
  protected question = this.context.data;

  constructor(
    @Inject(POLYMORPHEUS_CONTEXT)
    private readonly context: TuiDialogContext<boolean, string>,
  ) {}

  protected delete(): void {
    this.context.completeWith(true);
  }

  protected cancel(): void {
    this.context.completeWith(false);
  }
}
