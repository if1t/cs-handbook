import { NgModule } from '@angular/core';
import { DeleteFormComponent } from './delete-form.component';
import { TaigaUiSharedModule } from '../../../taiga-ui.shared.module';

@NgModule({
  declarations: [DeleteFormComponent],
  imports: [TaigaUiSharedModule],
  exports: [DeleteFormComponent],
})
export class DeleteFormModule {}
