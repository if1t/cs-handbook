import { TuiContextWithImplicit, TuiStringHandler } from '@taiga-ui/cdk';
import { IItem } from './select.schema';

export function defaultStringify(items: IItem[]): TuiStringHandler<TuiContextWithImplicit<number>> {
  const map = new Map(items.map(({ id, name }) => [id, name] as [number, string]));

  return ({ $implicit }: TuiContextWithImplicit<number>) => map.get($implicit) || '';
}
