import { Injectable } from '@angular/core';
import { TuiAlertService, TuiNotificationT } from '@taiga-ui/core';

@Injectable({
  providedIn: 'root',
})
export class AlertService {
  constructor(private readonly alerts: TuiAlertService) {}

  notify(message: string, status?: TuiNotificationT): void {
    this.alerts
      .open(message, {
        status: status ?? 'success',
      })
      .subscribe();
  }
}
