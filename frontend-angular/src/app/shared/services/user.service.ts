import { Injectable } from '@angular/core';

export enum ERole {
  ROLE_ADMIN = 'ROLE_ADMIN',
  ROLE_USER = 'ROLE_USER',
}

export interface IUser {
  username?: string;
  roles?: ERole[];
}

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private user: IUser | null = null;

  constructor() {
    const userString = localStorage.getItem('user');

    if (userString) {
      this.user = JSON.parse(userString);
    }
  }

  public setUser(user: IUser): void {
    this.user = user;
  }

  public getUser(): IUser | null {
    return this.user;
  }
}
