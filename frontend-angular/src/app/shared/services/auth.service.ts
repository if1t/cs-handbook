import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ICredentials } from '../../modules/login/login.schema';
import { tap } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from './alert.service';
import { UserService } from './user.service';
import { jwtDecode } from 'jwt-decode';

export const AUTH_ENDPOINT = 'api/auth';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    private http: HttpClient,
    private readonly _router: Router,
    private readonly _route: ActivatedRoute,
    private readonly _alertService: AlertService,
    private readonly _userService: UserService,
  ) {}

  public login(credentials: ICredentials): void {
    this.http
      .post(`${AUTH_ENDPOINT}/login`, credentials)
      .pipe(
        tap((response: any) => {
          const roles = (jwtDecode(response.token) as any).roles;
          const user = { username: response.username, roles };
          this._userService.setUser(user);
          localStorage.setItem('token', response.token);
          localStorage.setItem('user', JSON.stringify(user));
        }),
      )
      .subscribe({
        next: () => {
          const returnUrl = this._route.snapshot.queryParams['returnUrl'];
          void this._router.navigate([returnUrl ?? 'players']);
          this._alertService.notify('Авторизация успешно выполнена!');
        },
        error: (error: Error) => {
          this._alertService.notify('Неверный логин или пароль. Попробуйте еще раз.', 'error');
          console.error(error);
        },
      });
  }

  public register(credentials: ICredentials): void {
    this.http.post(`${AUTH_ENDPOINT}/register`, credentials).subscribe({
      next: () => {
        void this._router.navigate(['login']);
        this._alertService.notify('Регистрация успешно выполнена!');
      },
      error: (error: Error) => {
        this._alertService.notify('Ошибка при регитрации.', 'error');
        console.error(error);
      },
    });
  }

  public logout(): void {
    localStorage.removeItem('token');
    void this._router.navigate(['login']);
    this._alertService.notify('Выполнен выход из аккаунта', 'info');
  }

  public get isAuthenticated(): boolean {
    return !!this.token;
  }

  public get token(): string | null {
    return localStorage.getItem('token');
  }
}
