import { NgModule } from '@angular/core';
import {
  TuiButtonModule,
  TuiDataListModule,
  TuiFormatNumberPipeModule,
  TuiGroupModule,
  TuiHintModule,
  TuiHostedDropdownModule,
  TuiLinkModule,
  TuiLoaderModule,
  TuiSvgModule,
  TuiTextfieldControllerModule,
} from '@taiga-ui/core';
import {
  TuiAvatarModule,
  TuiInputModule,
  TuiIslandModule,
  TuiSelectModule,
  TuiToggleModule,
} from '@taiga-ui/kit';

@NgModule({
  declarations: [],
  imports: [
    TuiFormatNumberPipeModule,
    TuiIslandModule,
    TuiToggleModule,
    TuiLinkModule,
    TuiButtonModule,
    TuiInputModule,
    TuiTextfieldControllerModule,
    TuiSvgModule,
    TuiHintModule,
    TuiLoaderModule,
    TuiDataListModule,
    TuiSelectModule,
    TuiHostedDropdownModule,
    TuiGroupModule,
    TuiAvatarModule,
  ],
  exports: [
    TuiFormatNumberPipeModule,
    TuiIslandModule,
    TuiToggleModule,
    TuiLinkModule,
    TuiButtonModule,
    TuiInputModule,
    TuiTextfieldControllerModule,
    TuiSvgModule,
    TuiHintModule,
    TuiLoaderModule,
    TuiDataListModule,
    TuiSelectModule,
    TuiHostedDropdownModule,
    TuiGroupModule,
    TuiAvatarModule,
  ],
})
export class TaigaUiSharedModule {}
