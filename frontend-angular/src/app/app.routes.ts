import { Routes } from '@angular/router';
import { LoginComponent } from './modules/login/login.component';
import { AuthGuard } from './guards/auth.guard';
import { RegisterComponent } from './modules/register/register.component';
import { CabinetComponent } from './modules/cabinet/cabinet.component';

export const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
      },
      {
        path: 'login',
        component: LoginComponent,
      },
      {
        path: 'register',
        component: RegisterComponent,
      },
      {
        path: 'cabinet',
        component: CabinetComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'players',
        loadChildren: () => import('./modules/player/player.module').then((m) => m.PlayerModule),
        canActivate: [AuthGuard],
      },
    ],
  },
  { path: '**', redirectTo: '' },
];
