import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../shared/services/auth.service';

type IHeaders = { [p: string]: string | string[] } | undefined;

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private readonly _authService: AuthService) {}

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.includes('api')) {
      const headers = this._buildHeaders();

      return next.handle(
        req.clone({
          setHeaders: headers,
        }),
      );
    }

    return next.handle(req);
  }

  private _buildHeaders(): IHeaders {
    const headers: IHeaders = {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: '*/*',
    };
    const { token } = this._authService;

    if (token) {
      headers['Authorization'] = `Bearer_${token}`;
    }

    return headers;
  }
}
